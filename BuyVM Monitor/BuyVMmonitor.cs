﻿#region

using System;
using System.Collections;
using System.Data;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Map = System.Collections.Generic.Dictionary<string, object>;

#endregion

namespace BuyVM_Monitor
{
    internal static class BuyVMmonitor
    {
        private static NotifyIcon _icon;
        private static DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static DateTime _last = _epoch;
        private static Timer _timer;
        private static DataForm _dataForm;

        /// <summary>
        ///   The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            #region Generated code

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            #endregion

            PrepareNotifyIcon();
            PrepareTimer();
            PrepareForm();

            Application.Run();
        }

        /// <summary>
        ///   The form just has a DataGridView for showing a table of which items are in stock. It is shown when the application detects that any plans are in stock.
        /// </summary>
        private static void PrepareForm()
        {
            _dataForm = new DataForm
                            {
                                Visible = false
                            };

            // Without an appropriate onClose event, closing the form will leave the tray application running
            _dataForm.Closed += (sender, args) => Application.Exit();
        }

        /// <summary>
        ///   The timer periodically fires a method which checks the website to see if anything is in stock.
        /// </summary>
        private static void PrepareTimer()
        {
            // Create a timer
            _timer = new Timer
                         {
                             Enabled = true,
                             Interval = 10000
                         };
            _timer.Tick += CheckStock;
        }

        /// <summary>
        ///   The tray icon just has a context menu that allows the user to exit without resorting to TaskMan -> End process.
        /// </summary>
        private static void PrepareNotifyIcon()
        {
            // Create the tray icon
            _icon = new NotifyIcon
                        {
                            Visible = true,
                            Text = "BuyVM Monitor",
                            Icon = Icon.BuyVM
                        };

            // Make a context menu
            var m = new ContextMenu();
            m.MenuItems.Add("Quit", (sender, args) => Application.Exit());
            _icon.ContextMenu = m;
        }

        /// <summary>
        ///   Checks the stock status, records latest data and generates appropriate output (a balloon popup and a form) if there have been any interesting developments.
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void CheckStock(object sender, EventArgs e)
        {
            var stock_data = GetDataFromJson();

            var total = 0;
            var latest_update = _last;
            var result_table = ResultTable();

            foreach (Map plan in (IEnumerable) stock_data)
            {
                var current_time = ParseTimestamp(plan);

                AddPlan(result_table, plan);
                total += (int) plan["qty"];

                if (IsAfter(current_time, latest_update)) latest_update = current_time;
            }

            if (IsAfter(latest_update, _last)) PopBalloon(latest_update, total);
            if (total >= 0) DisplayResultForm(result_table);
        }

        /// <summary>
        ///   Stops the refresh loop and displays the stock data.
        /// </summary>
        /// <param name="result_table"> </param>
        private static void DisplayResultForm(DataTable result_table)
        {
            _timer.Enabled = false;
            _dataForm.table.DataSource = result_table;
            _dataForm.Visible = true;
        }

        /// <summary>
        ///   Shows a balloon notification when stock data is updated.
        /// </summary>
        /// <param name="latest_update"> </param>
        /// <param name="total"> </param>
        private static void PopBalloon(DateTime latest_update, int total)
        {
            _last = latest_update;
            _icon.ShowBalloonTip(5000, "BuyVM stock status updated:",
                                 total + " total plans in stock.", ToolTipIcon.Info);
            _icon.Text = "BuyVM Monitor last updated: " +
                         _last.ToLocalTime().ToShortTimeString() + " " + _last.ToLocalTime().ToShortDateString();
        }

        /// <summary>
        ///   Returns true if t is after t0.
        /// </summary>
        /// <param name="t"> </param>
        /// <param name="t0"> </param>
        /// <returns> </returns>
        private static bool IsAfter(DateTime t, DateTime t0)
        {
            return DateTime.Compare(t0, t) < 0;
        }

        /// <summary>
        ///   Converts a UTC timestamp into a DateTime instance.
        /// </summary>
        /// <param name="plan"> </param>
        /// <returns> </returns>
        private static DateTime ParseTimestamp(Map plan)
        {
            var timestamp = (int) ((Map) plan["lastupdate"])["time"];
            var current_time = _epoch.AddSeconds(timestamp);
            return current_time;
        }

        /// <summary>
        ///   Adds a given plan to the result table.
        /// </summary>
        /// <param name="result_table"> </param>
        /// <param name="plan"> </param>
        private static void AddPlan(DataTable result_table, Map plan)
        {
            var r = result_table.NewRow();
            r.SetField(0, plan["name"].ToString());
            r.SetField(1, (int) plan["qty"]);
            result_table.Rows.Add(r);
        }

        /// <summary>
        ///   Prepares a DataTable for holding the parsed stock data.
        /// </summary>
        /// <returns> </returns>
        private static DataTable ResultTable()
        {
            var result_table = new DataTable();
            result_table.Columns.Add("Plan Name", typeof (string));
            result_table.PrimaryKey = new[] {result_table.Columns[0]};
            result_table.Columns.Add("Quantity in stock", typeof (int));
            return result_table;
        }

        /// <summary>
        ///   Downloads the JSON stock data and parses it as an object tree (not really type safe).
        /// </summary>
        /// <returns> </returns>
        private static object GetDataFromJson()
        {
            var c = new WebClient();
            var json = c.DownloadString(@"http://doesbuyvmhavestock.com/automation.json");
            var parser = new JavaScriptSerializer();

            var stock_data = parser.DeserializeObject(json);
            return stock_data;
        }
    }
}